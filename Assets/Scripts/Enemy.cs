﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    // public PlayerController player_script;
    [SerializeField]
    private WaveType type;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _death;
    [SerializeField]
    private float patrolSpeed = 2f;
    [SerializeField]
    private LayerMask patrolWall;
    private float dir = -1;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Patrol();

        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir * Vector2.right, 0.5f, patrolWall);

        //Debug.Log(hit.transform.name);

        if (hit.collider)
        {
            dir *= -1;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<WaveScript>())
        {
            if (collision.GetComponent<WaveScript>().type != type)
            {

                StartCoroutine(DestroyEnemy());
                Destroy(gameObject);
                //isActive = false;
            }
            else
            {
                //isActive = true;
            }

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<PlayerController>())
        {
            //collision.collider.GetComponent<PlayerController>().PlayDeadSound();
            //Destroy(collision.collider.gameObject);
            collision.collider.GetComponent<PlayerController>().SwitchDead();
        }
    }

    private void OnDestroy()
    {
        
    }

    private void Patrol()
    {
        transform.Translate(Vector3.right * dir * Time.deltaTime * patrolSpeed);
    }

    private IEnumerator DestroyEnemy()
    {
        yield return new WaitForSeconds(1f);

        Destroy(gameObject, 0);
    }
}
