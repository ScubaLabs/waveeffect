﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterEnemy : MonoBehaviour
{

    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _shootAudio;
    [SerializeField]
    private float coolDownTime = 2f;

    private float shootTime = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (shootTime >= coolDownTime)
        {
            shootTime = 0;
            Shoot();
        }

        else
        {
            shootTime += Time.deltaTime;
        }
    }

    private void Shoot()
    {
        var bulletOne = Instantiate(bullet, transform.position, Quaternion.identity) as GameObject;
        var bulletTwo = Instantiate(bullet, transform.position, Quaternion.identity) as GameObject;

        bulletOne.GetComponent<Bullet>().ShootBullet(-1);
        bulletTwo.GetComponent<Bullet>().ShootBullet(1);

        bulletOne.transform.SetParent(transform);
        bulletTwo.transform.SetParent(transform);

        _audioSource.PlayOneShot(_shootAudio);
    }
}
