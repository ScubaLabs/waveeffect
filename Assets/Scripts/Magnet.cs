﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour {
    [SerializeField]
    private SpriteRenderer m_spriteRenderer;
    [SerializeField]
    private Color colorX;
    [SerializeField]
    private Color colorY;

    private WaveType type;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<WaveScript>())
        {
            if (!m_spriteRenderer.enabled)
                m_spriteRenderer.enabled = true;
            type = other.GetComponent<WaveScript>().type;

            if (type == WaveType.X)
            {
                m_spriteRenderer.color = colorX;
            }

            else
            {
                m_spriteRenderer.color = colorY;
            }
        }
    }
}
