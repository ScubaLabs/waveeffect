﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetoEnemy : MonoBehaviour
{
    [SerializeField]
    private WaveType type;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _death;
    [SerializeField]
    private SpriteRenderer m_ringSpriteRenderer;
    [SerializeField]
    private Color ringColor;
    [SerializeField]
    private float detectionArea = 1f;
    [SerializeField]
    private float pullSpeed = 1f;

    private bool isPlayerDead = false;
    private bool flag = false;

    private Transform player;

    // Use this for initialization
    void Start()
    {
        if (type == WaveType.Y)
        {
            m_ringSpriteRenderer.color = ringColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        var playerCollider = Physics2D.OverlapCircle(transform.position, detectionArea);
        if (playerCollider)
        {
            if (playerCollider.GetComponent<PlayerController>() && !flag)
            {
                playerCollider.GetComponent<PlayerController>().SetPlayerDead();
                player = playerCollider.transform;
                flag = true;
                isPlayerDead = true;
            }
            if (player)
                PullPlayer(player);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, detectionArea);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<WaveScript>())
        {
            if (other.GetComponent<WaveScript>().type != type)
            {
                //Destroy(gameObject, 0f);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<PlayerController>())
        {
            //Destroy(collision.collider.gameObject, 0);
        }
    }

    private void OnDestroy()
    {
        
    }

    private void PullPlayer(Transform player)
    {
        if (isPlayerDead)
            player.position = Vector2.MoveTowards(player.position, transform.position, Time.deltaTime * pullSpeed);
    }

}
