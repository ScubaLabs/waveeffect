﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    [SerializeField]
    private Transform target;
    [SerializeField]
    private float followSpeed = 1f;
    [SerializeField]
    private float offsetX = -2f;
    [SerializeField]
    private float offsetY = -3f;
    [SerializeField]
    private float maxY = 5f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (target)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.position.x + offsetX, Mathf.Clamp(target.position.y + offsetY, 4f, maxY), transform.position.z), Time.deltaTime * followSpeed);
        }
    }
}
