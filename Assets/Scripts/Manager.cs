﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager : MonoBehaviour {

    public static bool isPaused = false;

    [SerializeField]
    private GameObject pausedMenu;

    private void Start()
    {
        Time.timeScale = 1f;
        isPaused = false;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (Time.timeScale != 0 && pausedMenu)
            {
                Time.timeScale = 0;
                    pausedMenu.SetActive(true);
                isPaused = true;
            }

            else
            {
                Time.timeScale = 1f;
                if(pausedMenu)
                    pausedMenu.SetActive(false);
                isPaused = false;
            }
        }
    }

    public void OnClickPause()
    {

    }

    public void OnClickResume()
    {
        Time.timeScale = 1f;
        if (pausedMenu)
            pausedMenu.SetActive(false);
    }

    public void OnClickMainMenu()
    {
        SceneManager.LoadScene("menu");
    }

    public void OnClickPlay()
    {
        SceneManager.LoadScene(1);
    }

    public void OnClickExit()
    {
        Application.Quit();
    }

    public void OnClickAbout()
    {

    }
}
