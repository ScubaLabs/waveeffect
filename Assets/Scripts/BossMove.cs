﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMove : MonoBehaviour {

	[SerializeField] private float speed = 1f;
	private int direction = 1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up * speed * direction * Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D other){
			
		direction = -direction;

        if (other.GetComponent<PlayerController>())
        {
            other.GetComponent<PlayerController>().SwitchDead();
        }
	}
}
