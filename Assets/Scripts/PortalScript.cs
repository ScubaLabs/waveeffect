﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalScript : MonoBehaviour {

    private Transform player;
    private bool isPlayerDead = false;
    Transform pos;
    [SerializeField]
    private float detectionArea = 1f;
    [SerializeField]
    private float pullSpeed = 1f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(Vector3.forward, 1f);

        var playerCollider = Physics2D.OverlapCircle(transform.position, detectionArea);
        if (playerCollider)
        {
            if (playerCollider.GetComponent<PlayerController>() && !isPlayerDead)
            {
                playerCollider.GetComponent<PlayerController>().SetPlayerDead();
                player = playerCollider.transform;
                isPlayerDead = true;
            }
            if (player && Vector2.Distance(player.position, transform.position) > 0.2f)
                PullPlayer(player);

            else if(player)
                SceneManager.LoadScene(0);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, detectionArea);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<WaveScript>())
        {
            if (other.GetComponent<WaveScript>())
            {
                //Destroy(gameObject, 0f);
            }
        }
    }

    private void PullPlayer(Transform player)
    {
        
            player.position = Vector2.MoveTowards(player.position, transform.position, Time.deltaTime * pullSpeed);
    }
}
