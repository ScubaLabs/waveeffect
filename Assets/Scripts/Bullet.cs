﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField]
    private Rigidbody2D m_rigidbody;
    [SerializeField]
    private float speed = 1f;

    public Transform target;
    public float orbitDistance = 10.0f;
    public float orbitDegreesPerSec = 180.0f;

    // Use this for initialization
    void Start () {
        Invoke("DestroySelf", 5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider)
        if (other.collider.GetComponent<PlayerController>())
        {
                //other.collider.GetComponent<PlayerController>().PlayDeadSound();
                //Destroy(other.collider.gameObject, 0);
                other.collider.GetComponent<PlayerController>().SwitchDead();
            }

        Destroy(gameObject, 0);
    }

    public void ShootBullet(int dir)
    {
        m_rigidbody.velocity = Vector2.right * dir * speed;
    }

    private void DestroySelf()
    {
        Destroy(gameObject, 0f);
    }
}
