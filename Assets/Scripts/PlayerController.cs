﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public enum WaveType { X, Y }

[Serializable]
public class Player
{
    public bool isActive = true;
    public float speed = 2f;
    public float jumpForce = 20f;
    public float groundDistance = 0f;
    public LayerMask m_playerLayer;
    public WaveType waveType;
    public Color colorX;
    public Color colorY;
}

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private Player m_player;
    [SerializeField]
    private Rigidbody2D m_rigidbody;
    [SerializeField]
    private GameObject waveRed;
    [SerializeField]
    private GameObject waveBlue;
    [SerializeField]
    private SpriteRenderer m_spriteRenderer;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip[] _audioClips;
    [SerializeField]
    private Animator _animator;
    private GameObject currentWave;

    // Use this for initialization
    void Start()
    {
        currentWave = waveRed;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Manager.isPaused)
        {
            PlayerMovement();
            SwitchWaveType();
            FireWave();
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.collider.CompareTag("MovingPlatform"))
        {
            transform.SetParent(other.collider.transform);
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        transform.SetParent(null);
    }

    private void SwitchWaveType()
    {
        if (Input.GetButtonDown("Fire2") && m_player.isActive)
        {
            if (m_player.waveType == WaveType.X)
            {
                m_player.waveType = WaveType.Y;
                m_spriteRenderer.color = m_player.colorY;
                currentWave = waveBlue;
            }

            else
            {
                m_player.waveType = WaveType.X;
                m_spriteRenderer.color = m_player.colorX;
                currentWave = waveRed;
            }
        }
    }

    private void PlayerMovement()
    {
        if (m_player.isActive)
        {
            var movement = Input.GetAxis("Horizontal");
            transform.Translate(movement * m_player.speed * Time.deltaTime, 0f, 0f);

            if (Input.GetButtonDown("Jump") && IsGrounded())
            {
                m_rigidbody.AddForce(Vector2.up * m_player.jumpForce);
            }
        }
    }

    public void SwitchDead()
    {
        _animator.enabled = true;
    }

    private void FireWave()
    {
        if (Input.GetButtonDown("Fire1") && m_player.isActive)
        {
            Instantiate(currentWave, transform.position, Quaternion.identity);

            _audioSource.PlayOneShot(_audioClips[0]);
        }
    }

    public void SetPlayerDead()
    {
        m_player.isActive = false;
        //m_rigidbody.bodyType = RigidbodyType2D.Kinematic;
        m_rigidbody.gravityScale = 0;
        m_rigidbody.velocity = Vector2.zero;
    }

    public void PlayDeadSound()
    {
        if(_audioSource)
        _audioSource.PlayOneShot(_audioClips[1]);
    }

    public void DestroyPlayer()
    {
        SceneManager.LoadScene(1);
    }

    private bool IsGrounded()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -transform.up, m_player.groundDistance, m_player.m_playerLayer);

        if (hit.collider)
            return true;

        return false;
    }
}
