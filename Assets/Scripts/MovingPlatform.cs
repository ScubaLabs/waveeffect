﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    [SerializeField]
    private LayerMask patrolWall;
    [SerializeField]
    private float patrolSpeed = 2f;
    private float dir = -1;

    // Use this for initialization
    void Start () {
		
	}

    void Update()
    {
        Patrol();

        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir * Vector2.right, 0.5f, patrolWall);

        //Debug.Log(hit.transform.name);

        if (hit.collider)
        {
            dir *= -1;
        }
    }

    private void Patrol()
    {
        transform.Translate(Vector3.right * dir * Time.deltaTime * patrolSpeed);
    }
}
